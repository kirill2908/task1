const { books } = require('../../stores')

module.exports = async (req, res) => {

    const book = {
        author: req.body.author,
        text: req.body.text,
        date: new Date(req.body.date),
    }

    const result = await books.create(book)

    res.status(result.acknowledged ? 201 : 400).json(result)

}