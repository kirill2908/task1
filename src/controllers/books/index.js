const { asyncHandler } = require('../../server')

exports.books = {
    getDestribuition: asyncHandler(require('./getDestribuition')),
    getTop10Authors: asyncHandler(require('./getTop10Authors')),
    create: asyncHandler(require('./create')),
    update: asyncHandler(require('./update')),
    delete: asyncHandler(require('./delete')),
    read: asyncHandler(require('./read'))
}
