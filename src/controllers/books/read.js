const { books } = require('../../stores')

module.exports = async (req, res) => {

    const result = await books.read(req.query.id)

    res.status(result ? 200 : 404).json(result)

}