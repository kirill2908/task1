const { books } = require('../../stores')

module.exports = async (req, res) => {

    const book = {
        author: req.body.author,
        text: req.body.text,
        date: new Date(req.body.date),
    }

    const result = await books.update(req.query.id, book)

    res.status(result.acknowledged ? 200 : 404).json(result)

}