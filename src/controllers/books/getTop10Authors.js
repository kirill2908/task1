const { books } = require('../../stores')

module.exports = async (req, res) => {
   
    const authors = await books.getTop10Authors()
    res.json(authors)

}