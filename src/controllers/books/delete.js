const { books } = require('../../stores')

module.exports = async (req, res) => {

    const result = await books.delete(req.query.id)

    res.sendStatus(result ? 200 : 404)

}