const { MongoClient } = require('mongodb')
const { LoremIpsum } = require('lorem-ipsum')
const { uniqueNamesGenerator, names } = require('unique-names-generator')
const DateGenerator = require('random-date-generator')

const uri = process.env.MONGO_URL || 'mongodb://root:root@localhost:27017/'
const client = new MongoClient(uri)

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});

const config = { dictionaries: [names] }

const startDate = new Date(2017, 2, 2);
const endDate = new Date()

async function fillDb() {

  try {

    await client.connect();

    const database = client.db("booksDB");
    const books = database.collection("books");

    const countDocuments = await books.countDocuments()

    if (countDocuments) {
      return console.log('Collection is already filled!', countDocuments)
    }

    const emptyArray = new Array(200).fill(null)

    const docs = emptyArray.map(() => {
      return {
        author: uniqueNamesGenerator(config),
        text: lorem.generateParagraphs(7),
        date: DateGenerator.getRandomDateInRange(startDate, endDate)
      }
    })

    const options = { ordered: true };
    const result = await books.insertMany(docs, options);

    console.log(`${result.insertedCount} documents were inserted`);

  } finally {
    await client.close();
  }

}

fillDb().catch(console.dir);