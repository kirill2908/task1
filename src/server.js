const express = require('express')
const bodyParser = require('body-parser')

async function createServer(routes) {

  const app = express()
  const port = process.env.EXPRESS_PORT || 3000

  app.use(bodyParser.urlencoded({ extended: true }))

  app.use('/', routes)

  app.use((err, req, res, next) => {
    errorHandler(err, res)
  })

  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })

}

const errorHandler = (err, res) => {

  res.sendStatus(500)
  console.log(err)

}

const asyncHandler = (fn) => (req, res, next) => {
  return fn(req, res, next).catch(next)
}

module.exports = {
  asyncHandler,
  createServer
}