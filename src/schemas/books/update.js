module.exports = {
    id: {
        in: 'query',
        isMongoId: true,
        notEmpty: true,
        errorMessage: 'id should be in mongoId format and be present'
    },
    author: {
        in: 'body',
        isString: true,
        notEmpty: true,
        errorMessage: 'author should be an string and not empty'

    },
    text: {
        in: 'body',
        isString: true,
        notEmpty: true,
        errorMessage: 'text should be an string and not empty',
    },
    date: {
        in: 'body',
        custom : {
            options: (value) => {
               
                const dateTime = new Date(value).getTime()
                return !isNaN(dateTime)

            }
        },
        errorMessage: 'date should be an string in JSON date format and not empty',
    }
} 