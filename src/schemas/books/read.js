module.exports = {
    id: {
        in: 'query',
        isMongoId: true,
        notEmpty: true,
        errorMessage: 'id should be in mongoId format and be present'
    }
} 