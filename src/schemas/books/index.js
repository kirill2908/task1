exports.books = {
    create: require('./create'),
    update: require('./update'),
    delete: require('./delete'),
    read: require('./read')
}