const router = require('express').Router()
const { validate } = require('../middleware')

const controllers = require('../controllers')
const schemas = require('../schemas')

router.get('/books/top10Authors', controllers.books.getTop10Authors)
router.get('/books/destribuition', controllers.books.getDestribuition)
router.post('/books/create', validate(schemas.books.create), controllers.books.create)
router.put('/books/update', validate(schemas.books.update), controllers.books.update)
router.delete('/books/delete', validate(schemas.books.delete), controllers.books.delete)
router.get('/books/read', validate(schemas.books.read), controllers.books.read)

module.exports = router;