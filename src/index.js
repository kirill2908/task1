const { connectMongo } = require('./books')
const { createServer } = require('./server')

const routes = require('./routes')

connectMongo().then(() => createServer(routes))