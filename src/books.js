const { MongoClient, ObjectId} = require('mongodb')

const uri = process.env.MONGO_URL || 'mongodb://root:root@localhost:27017/'
const client = new MongoClient(uri)
const database = client.db('booksDB');
const books = database.collection('books');

async function connectMongo() {

    await client.connect();
    await books.createIndex({ author: 1 });
    await books.createIndex({ date: 1 });

}

module.exports = { 
    books,
    connectMongo,
    ObjectId
}