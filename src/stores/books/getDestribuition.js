const { books } = require('../../books')

module.exports = async () => {
 
    const today = new Date()
    const start = new Date()
    start.setMonth(today.getMonth() - 24)

    const pipeline = [
        {
            $match: {
                date: { $gte: start, $lt: today }
            }
        },
        {
            $group: {
                _id: {
                    year: { $year: '$date' },
                    month: { $month: '$date' },
                },
                count: { $sum: 1 }
            }
        },
        {
            $addFields: {
                year: '$_id.year',
                month: '$_id.month'
            }
        },
        {
            $sort: {
                'year': 1,
                'month': 1
            }
        },
        {
            $project: { _id: 0, year: 1, month: 1, count: 1 }
        }
    ]

    return books.aggregate(pipeline).toArray()

}