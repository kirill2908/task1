exports.books = {
    create: require('./create'),
    update: require('./update'),
    delete: require('./delete'),
    read: require('./read'),
    getDestribuition: require('./getDestribuition'),
    getTop10Authors: require('./getTop10Authors')
}