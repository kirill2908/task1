const { books } = require('../../books')

module.exports = async () => {
 
    const pipeline = [
        {
            $group: {
                _id: { author: '$author' },
                countText: { $count: {} }
            }
        },
        {
            $sort: {
                'countText': -1
            }
        },
        {
            $limit: 10
        },
        {
            $addFields: {
                author: '$_id.author'
            }
        },
        {
            $project: { _id: 0, author: 1, countText: 1 }
        }
    ];

    return books.aggregate(pipeline).toArray()

}