const { books, ObjectId } = require('../../books')

module.exports = async (id, book) => {

    const query = { '_id': new ObjectId(id) }
    const result = await books.updateOne(query, { $set: book }, { upsert: true })

    return {
        found: result.matchedCount > 0,
        modified: result.modifiedCount > 0
    }

}