const { books, ObjectId } = require('../../books')

module.exports = async (id) => {

    const query = { '_id': new ObjectId(id) }
    const result = await books.findOne(query)

    return result

}