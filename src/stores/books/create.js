const { books } = require('../../books')

module.exports = async (book) => {
    return await books.insertOne(book)
}